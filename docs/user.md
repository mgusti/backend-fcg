# User API Spec

## Register User API

Endpoint : POST /api/users

Request Body :

```json
{
  "username": "gusti",
  "password": "rahasia",
  "user": "Muhammad Gusti Kurniawan"
}
```

Response Body Success :

```json
{
  "data": {
    "username": "gusti",
    "user": "Muhammad Gusti Kurniawan"
  }
}
```

Response Body Error :

```json
{
  "errors": "Username already registered"
}
```

## Login User API

Endpoint : POST /api/users/login

Request Body :

```json
{
  "username": "gusti",
  "password": "rahasia"
}
```

Response Body Success :

```json
{
  "data": {
    "id_user": "unique-id",
    "token": "unique-token"
  }
}
```

Response Body Error :

```json
{
  "errors": "Username or password is wrong"
}
```

## Update User API

Endpoint : PATCH /api/users/current

Headers :

- Authorization : token

Request Body :

```json
{
  "username": "gusti2",
  "password": "new password"
}
```

Response Body Success :

```json
{
  "data": {
    "username": "gusti",
    "user": "Muhammad Gusti Kurniawan"
  }
}
```

Response Body Error :

```json
{
  "errors": "User Length Max 100"
}
```

## Get User API

Endpoint : GET /api/users/current

Headers :

- Authorization : token

Request Body Success:

```json
{
  "data": {
    "username": "gusti",
    "name": "Muhammad Gusti Kurniawan"
  }
}
```

Request Body Error :

```json
{
  "errors": "Unauthorized"
}
```

## Logout User API

Endpoint : DELETE /api/users/logout

Headers :

- Authorization : token

Response Body Success:

```json
{
  "data": "OK"
}
```

Response Body Error :

```json
{
  "errors": "Unauthorized"
}
```
